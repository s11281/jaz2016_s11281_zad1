package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.Math;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@WebServlet("/hello")
public class HelloServlet extends HttpServlet{

	private static final long serialVersionUID = 1L;

	
	public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException
	{	if(request.getParameter("kwota")==null 
			|| request.getParameter("kwota").equals("")
			|| request.getParameter("ileRat")==null
			|| request.getParameter("ileRat").equals("")
			|| request.getParameter("profit")==null
			|| request.getParameter("profit").equals("")
			|| request.getParameter("oplataStala")==null
			|| request.getParameter("oplataStala").equals("")
			|| request.getParameter("typRat")==null
			)
				{
					response.sendRedirect("/index.jsp");
				}else
				{	float kwota=0;
					int ileRat=0;
					float profit=0;
					float oplataStala=0;
					float rata=0;
					String typRat=request.getParameter("typRat");
					java.text.DecimalFormat df=new java.text.DecimalFormat();
					df.setMaximumFractionDigits(2);
					df.setMinimumFractionDigits(2);
				
				try 
				{
					kwota = Float.parseFloat(request.getParameter("kwota"));
					}catch (NumberFormatException e){response.sendRedirect("/index.jsp");};
				try 
				{
					ileRat = Integer.parseInt(request.getParameter("ileRat"));
					}catch (NumberFormatException e){response.sendRedirect("/index.jsp");};
				try 
				{
					profit = Float.parseFloat(request.getParameter("profit"));
					}catch (NumberFormatException e){response.sendRedirect("/index.jsp");};
				try 
				{
					oplataStala = Float.parseFloat(request.getParameter("oplataStala"));
					}catch (NumberFormatException e){response.sendRedirect("/index.jsp");};
				if(kwota==0 || ileRat==0 || profit==0)
					{
						response.sendRedirect("/index.jsp");
					}else 	
					{	
						float q=1+((profit/100)/12);
						PrintWriter out = response.getWriter();
						
						response.setContentType("text/html");
						out.println("<h1>Harmonogram rat</h1>");
						out.println("<table border=1>");
						out.println("<tr><th>Numer raty</th><th>Czesc kapita�owa</th><th>Czesc odsetkowa</th><th>Oplata sta�a</th><th>Ca�a rata</th></tr>");
						
						rata=(float)(kwota*Math.pow(q,ileRat)*(q-1)/(Math.pow(q,ileRat)-1));
						
						for(int i=1;i<=ileRat;i++)
						{	
							float czKapital;
							float czOdsetkowa;
							if(typRat.equals("malejace")){
								czKapital=kwota/ileRat;
								czOdsetkowa=(kwota-((i-1)*czKapital))*profit/100/12;
							}else{
								czKapital=kwota/ileRat;
								czOdsetkowa=rata-czKapital;
							};							;
							
							out.println("<tr><td>"+i+"</td><td>"+df.format(czKapital)+"</td><td>"+df.format(czOdsetkowa)+"</td><td>"+df.format(oplataStala)+"</td><td>"+df.format(czKapital+czOdsetkowa+oplataStala)+"</td></tr>");
						};
						out.println("</table>");
					
					}
				}
			}
	}

